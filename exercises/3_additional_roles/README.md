# Exercise 3 - Adding Additional Roles: Leveraging Ansible roles in GitLab to automate enterprise deployments

## Section 3.1: Updated Inventory File
- Add IP's for the Applicaiton & Runner VM's 
[Link to inventory.yml](inventory.yml)

```yml
      application:
        hosts: 
          x.x.x.x:
        vars:
          function: application
      runner:
        hosts:
          x.x.x.x:
        vars:
          function: runner

```

## Section 3.2: View the Update Roles
We now have all 4 Ansible roles for this project. Each of these roles are a set of tasks to configure the host. Which are a collection of task, files, handlers, and vars.
- ActiveGate
- EasyTravel
- OneAgent
- OneAgentCtl

### Section 3.2  Edit group_vars called activegate.yml in Install & Uninstall directories
The reason for the uninstall and install group_vars is to allow us to reuse the same ansible roles for install or uninstall. Only having to pass different vars to deploy or remove depending on the variable state which corresponds with install or uninstall on the Gitlab pipeline. 
1. Install Directory --> activegate.yml
```yml
---
    dynatrace_environment_url: "{{ url }}"
    dynatrace_paas_token: "{{ paas }}"
    activegate_network_zone: hot_session
    # dynatrace_activegate_install_args:
    #     PROXY: myproxy:888
    #     INSTALL: /application/dynatrace
    #     LOG: /application/dynatrace/logs
    #     CONFIG: /application/dynatrace/config
    #     TMP: /application/dynatrace/tmp
    #     PACKAGES_DIR: /application/dynatrace/packages
```
2. Uninstall Directory --> activegate.yml
```yml
---
activegate_install: "false"
```

### Section 3.3 Edit Playbooks for ActiveGate, EasyTravel, and Environment Configuration
Navigate to playbooks directory and edit the following ansible playbooks
1. Edit `activegate.yml` playbook
    - These playbooks are just targeting the application group determined by the inventory group application

```yml
---
- hosts: application
  remote_user: root
  gather_facts: yes
  become: yes  
  vars_files:
    - "../group_vars/{{ state }}/activegate.yml"
  roles:
    - role: ../roles/activegate
```

2. Edit `easytravel.yml` playbook
```yml
---
- hosts: application
  remote_user: root
  gather_facts: yes
  become: yes  
  roles:
    - role: ../roles/easytravel
```

3. `Edit` environment_config.yml playbook
This playbook is different than the rest we have used in this lab. All of the task are defined in the playbook.

```yml
---
- hosts: runner
  remote_user: root
  gather_facts: yes
  become: yes
  tasks:
      - name: Get all autotags
        uri:
            url: "{{ dynatrace_tenant_url }}/api/config/v1/autoTags?Api-token={{ dynatrace_api_token }}"
            method: GET
            headers:
            return_content: no
            status_code: 200, 201, 204
        register: autotagsresponse

      - name: Set autoTagId for support
        set_fact:
            tags_support_id: "{{ item }}"
        loop: "{{ autotagsresponse.json | json_query(autoTag_query) }}"
        vars:
            autoTag_query: "values[?name=='support'].id"
        when: item != ""

      - name: Create autoTag for support
        uri:
            url: "{{ dynatrace_tenant_url }}/api/config/v1/autoTags?Api-token={{ dynatrace_api_token }}"
            method: POST
            body_format: json
            body: '{
            "name": "support",
            "rules": [
                {
                "type": "PROCESS_GROUP",
                "enabled": true,
                "valueFormat": "{Host:Environment:Support}",
                "propagationTypes": [
                    "PROCESS_GROUP_TO_HOST",
                    "PROCESS_GROUP_TO_SERVICE"
                ],
                "conditions": [
                    {
                    "key": {
                        "attribute": "HOST_CUSTOM_METADATA",
                        "dynamicKey": {
                        "source": "ENVIRONMENT",
                        "key": "Support"
                        },
                        "type": "HOST_CUSTOM_METADATA_KEY"
                    },
                    "comparisonInfo": {
                        "type": "STRING",
                        "operator": "EXISTS",
                        "value": null,
                        "negate": false,
                        "caseSensitive": null
                    }
                    }
                ]
                }
            ]
                }'
            return_content: no
            status_code: 200, 201, 204
        when: tags_support_id is not defined

      - name: Set autoTagId for App
        set_fact:
            tags_app_id: "{{ item }}"
        loop: "{{ autotagsresponse.json | json_query(autoTag_query) }}"
        vars:
            autoTag_query: "values[?name=='App'].id"
        when: item != ""

      - name: Create autoTag for App
        uri:
            url: "{{ dynatrace_tenant_url }}/api/config/v1/autoTags?Api-token={{ dynatrace_api_token }}"
            method: POST
            body_format: json
            body: '{
            "name": "App",
            "rules": [
                {
                "type": "PROCESS_GROUP",
                "enabled": true,
                "valueFormat": "{DockerContainerGroupInstance:ContainerName}",
                "propagationTypes": [
                    "PROCESS_GROUP_TO_SERVICE"
                ],
                "conditions": [
                    {
                    "key": {
                        "attribute": "DOCKER_CONTAINER_NAME",
                        "type": "STATIC"
                    },
                    "comparisonInfo": {
                        "type": "STRING",
                        "operator": "EXISTS",
                        "value": null,
                        "negate": false,
                        "caseSensitive": null
                    }
                    }
                ]
                }
            ]
                }'
            return_content: no
            status_code: 200, 201, 204
        when: tags_app_id is not defined

      - name: Get all Naming Conditions
        uri:
            url: "{{ dynatrace_tenant_url }}/api/config/v1/conditionalNaming/processGroup?Api-token={{ dynatrace_api_token }}"
            method: GET
            return_content: no
            status_code: 200, 201, 204
        register: pgresponse

      - name: Set Conditional Naming PG for EasyTravel
        set_fact:
            pg_naming_id: "{{ item }}"
        loop: "{{ pgresponse.json | json_query(autoTag_query) }}"
        vars:
            autoTag_query: "values[?name=='Easytravel'].id"
        when: item != ""


      - name: Create EasyTravel Conditional Naming PG
        uri:
            url: "{{ dynatrace_tenant_url }}/api/config/v1/conditionalNaming/processGroup?Api-token={{ dynatrace_api_token }}"
            method: POST
            body_format: json
            body: '{
                    "type": "PROCESS_GROUP",
                    "nameFormat": "{DockerContainerGroupInstance:StrippedImageName/[^\\/]*$}",
                    "displayName": "EasyTravel",
                    "enabled": true,
                    "rules": [
                    {
                        "key": {
                            "attribute": "DOCKER_STRIPPED_IMAGE_NAME",
                            "type": "STATIC"
                        },
                        "comparisonInfo": {
                            "type": "STRING",
                            "operator": "EXISTS",
                            "value": null,
                            "negate": false,
                            "caseSensitive": null
                        }
                    }
                    ]
                }'
            return_content: no
            status_code: 200, 201, 204
        when: pg_naming_id is not defined

      - name: Update EasyTravel
        uri:
            url: "{{ dynatrace_tenant_url }}/api/config/v1/conditionalNaming/processGroup/{{ pg_naming_id }}?Api-token={{ dynatrace_api_token }}"
            method: PUT
            body_format: json
            body: '{
                    "type": "PROCESS_GROUP",
                    "nameFormat": "{DockerContainerGroupInstance:StrippedImageName/[^\\/]*$}",
                    "displayName": "EasyTravel",
                    "enabled": false,
                    "rules": [
                    {
                        "key": {
                        "attribute": "DOCKER_STRIPPED_IMAGE_NAME",
                        "type": "STATIC"
                        },
                        "comparisonInfo": {
                        "type": "STRING",
                        "operator": "EXISTS",
                        "value": null,
                        "negate": false,
                        "caseSensitive": null
                        }
                    }
                    ]
                }'
            return_content: no
            status_code: 200, 201, 204
        when: pg_naming_id is defined
```

### Section 3.4 Update the Pipline (.gitlab-ci.yml)
We are going to now update the pipeline to include all the new roles we have just added in our project.

1. Update the stages:
```yml
stages:
  - check
  - deploy
  - configuration
  - uninstall
```

2. Add deploy-activegate stage
```yml
deploy-activegate:
  <<: *ansible
  stage: deploy
  script:
    - ansible-playbook playbooks/activegate.yml --extra-vars "ansible_user=$user ansible_password=$pass ansible_sudo_pass=$pass dynatrace_environment_url=$url dynatrace_paas_token=$paas_token state=$state"
  variables:
    state: install
  when: manual
```

3. Add deploy-easytravel stage
```yml
deploy-easytravel:
  <<: *ansible
  stage: deploy
  script:
    - ansible-playbook playbooks/easytravel.yml --extra-vars "ansible_user=$user ansible_password=$pass ansible_sudo_pass=$pass"
  when: manual
```

4. Add configure OneAgentctl stage
```yml
configure-oneagent:
  <<: *ansible
  stage: configuration
  script:
    - ansible-playbook playbooks/oneagentctl.yml --extra-vars "ansible_user=$user ansible_password=$pass ansible_sudo_pass=$pass"
  when: manual
  ```

5. Add configure environment stage
```yml
configure-environment:
  <<: *ansible
  stage: configuration
  script:
    - ansible-playbook playbooks/environment_config.yml --extra-vars "ansible_user=$user ansible_password=$pass ansible_sudo_pass=$pass dynatrace_tenant_url=$url dynatrace_api_token=$api_token" -v
  when: manual
  ```
6. Add uninstall-activegate stage
```yml
uninstall-activegate:
  <<: *ansible
  stage: uninstall
  script:
    - ansible-playbook playbooks/activegate.yml --extra-vars "ansible_user=$user ansible_password=$pass ansible_sudo_pass=$pass state=$state"
  variables:
    state: uninstall
  when: manual
```


**NOTE**
####
> If you want to save time or having issues you can grab the full gitlab-ci.yml file from 4_final branch
---
 

### Commit the Changes to the existing branch and run the pipeline
Please view the pipeline and trigger the jobs that require manual triggers.

![Create Project](0_configure_env/images/pipeline_overview.png)
