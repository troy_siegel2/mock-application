# Exercise 2 - Variable: Leveraging variables in Ansible & GitLab

## Section 2.0: Updated Inventory File
- Add IP's for the Applicaiton & Runner VM's
[Link to inventory.yml](inventory.yml)

```yml
      application:
        hosts: 
          x.x.x.x:
      runner:
        hosts:
          x.x.x.x:
```

## Section 2.1: Uninstall OneAgent
In the previous exercise we deployed the OneAgent we will first need to run the uninstall role. Go to CI/CD and click the latest pipeline and click uninstall job. If pipeline has not ran click CI/CI --> Run Pipline --> Select Branch `2_variables`


## Section 2.2: Adding OneAgentctl 
### 1. View the Roles Directory
You will now see that there is an additional role for the oneagentctl.

### 2. Edit Inventory File - Yet again...
We are adding a variable called function to each of the groups. This will allow us to dictate which Oneagentctl properties are going to be applied to each host during the configuration phase. 

``` yml
all:
    hosts:
    vars:
        ansible_python_interpreter: /usr/bin/python3
    children:
      application:
        hosts: 
          x.x.x.x:
        vars:
          function: application
      runner:
        hosts:
          x.x.x.x:
        vars:
          function: runner
```

### 3.  Edit group_vars oneagentctl
1. Inside of oneagentctl directory edit the two files application.yml & runner.yml
2. runner.yml add the following lines:
    - Each one of these is a seperate variable which gets applied when the OneAgentctl ansible role runs.
    
    ``` yml
        ---
        function: runner
        host_group: "runner_host_group"
        bio: "bio"
        ci: "ci"
        support: "support"
        tier: 1
        network_zone: "hot_session"
        set_infra_only: "false"
        set_host_name: "runner"
    ```
3. application.yml
    ``` yml
        ---
        function: application
        host_group: "application_host_group"
        bio: "bio"
        ci: "ci"
        support: "support"
        tier: 1
        network_zone: "hot_session"
        set_infra_only: "false"
        set_host_name: "application"
    ```
**NOTE**
####
> What are group_vars, is an Ansible-specific folder as part of the repository structure. This folder contains YAML files created to have data models, and these data models apply to all the devices listed in the hosts. We can apply group variables to logical functions and hardware platforms.
---

### 3.1  Viewing the oneagentctl task
If you would like to view the full OneAgentCtl task --> [Link to OneAgentCtl task](roles/oneagentctl/tasks)

```yml
---
- name: set install params
  set_fact:
    oneagent_install_params: "{{ item.key }}={{ item.value }}"
  with_dict: "{{ dynatrace_oneagent_install_args }}"
  register: oneagent_install_param

- name: check if oneagent is installed | linux
  stat:
    path: "{{ dynatrace_oneagent_install_artifact }}"
  register: dynatrace_oneagent_state_file_linux
  when: ansible_system|lower == "linux"

- name: post-installation OneAgent configuration | linux
  command: "./oneagentctl {{ oneagent_install_param.results | map(attribute='ansible_facts.oneagent_install_params') | join(' ') }} --restart-service"
  args:
    chdir: "{{ oneagent_linux_dir }}"
  become: true
  become_user: root   
  when: 
    - ansible_system|lower == "linux"

```

### 4.  Edit the OneAgentctl Playbook  
Edit the playbooks/oneagentctl.yml playbook with the task to include the oneagentctl role.
[Link to playbooks](playbooks)
```yml
---
- hosts: application, runner
  remote_user: root
  gather_facts: yes
  become: yes  
  vars_files:
    - "../group_vars/oneagentctl/{{ function }}.yml"
  roles:
    - role: ../roles/oneagentctl
```

---
**NOTE**
####
> Hey, wait just one second ... did we just put variables in two seperate places?
Yes, we did. Variables can live in quite a few places.  Just to name a few: +

- vars directory
- defaults directory
- group_vars directory
- In the playbook under the `vars:` section
- In any file which can be specified on the command line using the `--extra_vars` option

To get a better understanding please read up on [variable precedence](http://docs.ansible.com/ansible/latest/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable) to understand where to define variables and which locations take precedence.
---

### 5. Adding OneAgentctl to the Pipeline
1. Edit the `.gitlab-ci.yml` file to include a *configuration* stage
```yml
stages:
  - check
  - deploy
  - configuration
  - uninstall
```
2. Between deploy-oneagent & uninstall-oneagent add configure-oneagent
```yml
# Configure Stage - OneAgentctl
configure-oneagent:
  <<: *ansible
  stage: configuration
  script:
    - ansible-playbook playbooks/oneagentctl.yml --extra-vars "ansible_user=$user ansible_password=$pass ansible_sudo_pass=$pass"
  when: manual
```

### 6. Commit and view the pipeline
Commit the changes to 2_variables

```
TASK [../roles/oneagentctl : set install params] *******************************
ok: [104.131.31.68] => (item={'key': '--set-host-group', 'value': 'application_host_group'})
ok: [104.131.31.68] => (item={'key': '--set-host-property', 'value': 'BIO=bio'})
ok: [104.131.31.68] => (item={'key': '--set-host-property', 'value': 'CI=ci'})
ok: [104.131.31.68] => (item={'key': '--set-host-property', 'value': 'Support=support'})
ok: [104.131.31.68] => (item={'key': '--set-host-property', 'value': 'Tier=1'})
ok: [104.131.31.68] => (item={'key': '--set-network-zone', 'value': 'hot_session'})
ok: [104.131.31.68] => (item={'key': '--set-host-name', 'value': 'application'})


```

---
## Info:
What is group_vars:
Group_vars is an Ansible-specific folder as part of the repository structure. This folder contains YAML files created to have data models, and these data models apply to all the devices listed in the hosts. We can apply group variables to logical functions and hardware platforms.

Taking a look at the Inventory.yml file we can see we have added an additional variable for each group called function. Which is leveraged in the oneagentctl.yml playbook to point to which group_vars should be set for the host.


### 7. Edit the Oneagentctl group_var file to add an additonal variable 
Editing the group_vars select both application.yml & runner.yml
1. group_vars --> application.yml & runner.yml
Example
```
host_property: "test"
```

Now that we created a new variable called host_property with a value test. We will now need to assign that variable in the OneAgentctl role.
2. roles --> oneagentctl --> vars --> main.yml
Example for the above variable `host_property`
```
- --set-host-property: 'Host_Property={{ host_property }}'
```

### 8. Commit the changes and view the CI/CD Pipeline

Recap of Pipeline Run with additional variable: (This will run all the properties)
```
ok: [104.131.31.68] => (item={'key': '--set-host-property', 'value': 'test'})
```
### 9. Completed

To continue go to branch 3_additional_roles - which will build off of this to include even more functionality into the pipeline.


