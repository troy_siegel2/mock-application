# Exercise 1 - Running your first playbook 
---
##### For this exercise we will be running the OneAgent role to deploy the OneAgent to the target application server.
---

## Section 1.1: Updated Inventory File
- Add IP's for the Applicaiton & Runner VM's 
[Link to inventory.yml](inventory.yml)

```yml
      application:
        hosts: 
          x.x.x.x:
        vars:
          function: application
      runner:
        hosts:
          x.x.x.x:
        vars:
          function: runner
```

- Commit the changes to the 1_run_playbook

What is a playbook? 
- It is a blueprint of automation tasks
- Playbooks are written in YAML format. Playbooks are one of the core features of Ansible and tell Ansible what to execute. 


Now that you are viewing `oneagent.yml`, let's begin by understanding what each line accomplishes:


```yml
---
- hosts: all
  name: Install OneAgent
  become: yes
```

- `---` Defines the beginning of YAML
- `hosts: all` Defines the host group in your inventory on which this play will run against
- `name: Install OneAgent` This describes the play
- `become: yes` Enables user privilege escalation.  The default is sudo, but su, pbrun, and [several others](http://docs.ansible.com/ansible/become.html) are also supported.


## Section 1.1: Adding Roles to Your Play

```yml
 roles: Deploy Dynatrace Oneagent
   role: ../roles/oneagent
```

- `roles:` Roles are made of tasks, vars, files, handlers. That allow you to group your automation project into roles to easily reuse them.

- `- role:` Role name can be a simple name, or it can be a fully qualified path. 


## Section 1.2: CI/CD Pipeline
Viewing the CI/CD Pipeline - You will see that the OneAgent is deployed to both the VM's in the inventory file.

![Completed Pipeline Run](images/pipeline_successful.png)


```
PLAY RECAP *********************************************************************
104.131.31.68              : ok=31   changed=5    unreachable=0    failed=0    skipped=19   rescued=0    ignored=0   
45.55.37.239               : ok=31   changed=5    unreachable=0    failed=0    skipped=19   rescued=0    ignored=0   
```

## Section 1.3: Completed
Continue to branch 2_variables
